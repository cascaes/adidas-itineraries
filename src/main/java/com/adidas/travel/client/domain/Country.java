package com.adidas.travel.client.domain;

import lombok.Data;

@Data
public class Country {

    private String name;

    private String isoCode;
}
