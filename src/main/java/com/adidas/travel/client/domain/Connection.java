package com.adidas.travel.client.domain;

import lombok.Data;

@Data
public class Connection {

    private Route fullRoute;

    private Route connectionRoute;
}
